# Angular website made with NO-SQL and NEO4J
This is a website where you can view, review and create games. It is also possible to create and view articles, make friends and make lists of games.

More information about the website structure: https://cswf-qiang.herokuapp.com/about

Note: this website is built with limited resources, so there will be missing functionality when the databases are terminated.

# Design 

![Article List](/Screenshots/Articles.png "Article list")
![Details](/Screenshots/Detail.png "Details")

