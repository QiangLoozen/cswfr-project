describe('Dashboard test', () => {
  it('Visits the dashboard page', () => {
    cy.visit('/');
    cy.contains('All Games');
    cy.contains('Sign in');
    cy.get('.navbar-nav').should('have.length', 1);
    cy.get('.navbar-nav').should('not.contain', 'User overview');
    cy.contains('This app is running');
    cy.should('not.contain','Enable recommendations:');
    cy.contains('Rocket League');
    cy.contains('Battlefield 2042');
    cy.get('.listItem').should('have.length', 2);
  });
});
