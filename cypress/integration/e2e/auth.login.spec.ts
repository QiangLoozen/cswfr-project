/**
 *
 */
describe('Auth login', () => {
  const mockUserData =  {
    _id: "u1",
    username: "Creator",
    email: "creator@email.com",
    password: "$2b$10$gV2uOT3FJ.SLjJy7sQCXguMZpmTiXNFMZE5vE7FBaRjH6PJhu9gX6",
    birthdate: "2000-12-30T00:00:00.000Z",
    role: "creator",
    games: [],
    lists: [],
    __v: 0,
    token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MWFhNmRiYjNhNzZjNDhjYjM1OGQxNTciLCJpYXQiOjE2Mzg2Mzk2NTV9.9ybnhvuTaKzWODbmjvww3CSWBaz4gEQ_FUEPOOTydeA"
  }

  it('visits the /login page', () => {
    cy.visit('/login');
    cy.contains('Login');
    cy.get('h1').should('have.text', 'Login');
    cy.contains('E-mail');
    cy.contains('Password');
    cy.contains('Login');
    cy.get('button#loginBtn').should('have.text', 'Login');
  });

  it('should show error message when email is invalid', () => {
    cy.visit('/login');
    cy.get('input#email').type('some invalid email');
    cy.get('input#password').click();
    cy.get('#emailerror').should(
      'have.text',
      ' Email is invalid. '
    );
  });

  it('should return a token when login is valid', () => {
    cy.visit('/login');
    cy.get('input#email').type('creator@email.com');
    cy.get('input#password').type('Admin123');
    cy.get('button#loginBtn')
      .click()
      .should(() => {
        expect(localStorage.getItem('currentuser')).to.eq(
          JSON.stringify(mockUserData)
        );
      });
    cy.location('pathname').should('eq', '/dashboard');
    cy.get('button#user').should('have.text', 'Creator');
    cy.get('.navbar-nav').should('contain', 'User overview');
  });
});
