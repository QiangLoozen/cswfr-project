describe('Articles test', () => {
  const mockUserData =  {
    _id: "u1",
    username: "Creator",
    email: "creator@email.com",
    password: "$2b$10$gV2uOT3FJ.SLjJy7sQCXguMZpmTiXNFMZE5vE7FBaRjH6PJhu9gX6",
    birthdate: "2000-12-30T00:00:00.000Z",
    role: "creator",
    games: [],
    lists: [],
    __v: 0,
    token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MWFhNmRiYjNhNzZjNDhjYjM1OGQxNTciLCJpYXQiOjE2Mzg2Mzk2NTV9.9ybnhvuTaKzWODbmjvww3CSWBaz4gEQ_FUEPOOTydeA"
  }

    it('Visits the article page', () => {
      cy.visit('/articles');
      cy.contains('Articles');
      cy.contains('Sign in');
      cy.get('.navbar-nav').should('have.length', 1);
      cy.get('.navbar-nav').should('not.contain', 'User overview');
      cy.contains('Article1');
      cy.contains('Article2');
      cy.contains('news');
      cy.get('.card').should('have.length', 2);
    });

    it('Should update an article', () => {
      // Login
      cy.visit('/login');
      cy.get('input#email').type('creator@email.com');
      cy.get('input#password').type('Admin123');
      cy.get('button#loginBtn')
        .click()
        .should(() => {
          expect(localStorage.getItem('currentuser')).to.eq(
            JSON.stringify(mockUserData)
          );
        });
      cy.location('pathname').should('eq', '/dashboard');
      cy.get('button#user').should('have.text', 'Creator');
      cy.get('.navbar-nav').should('contain', 'User overview');
  
      // Update
      cy.visit('/articles');
      cy.contains('Article1').click();
      cy.contains('Overview');
      cy.contains("Edit");
      cy.get('#editBtn').should('have.text', 'Edit').click();
      cy.get('#title').clear().type('Updated Article');
      cy.get('#subtitle').clear().type('Updated Subtitle');
      cy.get('button').contains('Save').click();
      cy.location('pathname').should('eq', '/articles/a1');
    });

    it('Should delete an article', () => {
      // Login
      cy.visit('/login');
      cy.get('input#email').type('creator@email.com');
      cy.get('input#password').type('Admin123');
      cy.get('button#loginBtn')
        .click()
        .should(() => {
          expect(localStorage.getItem('currentuser')).to.eq(
            JSON.stringify(mockUserData)
          );
        });
      cy.location('pathname').should('eq', '/dashboard');
      cy.get('button#user').should('have.text', 'Creator');
      cy.get('.navbar-nav').should('contain', 'User overview');
  
      // Remove
      cy.visit('/articles');
      cy.contains('Article1').click();
      cy.get('#editBtn').should('have.text', 'Edit').click();
      cy.get('#removeBtn').should('have.text', 'Remove').click();
      cy.location('pathname').should('eq', '/articles');
    })
  });
  