//
// This server will be started by Protractor in end-to-end tests.
// Add your API mocks for your specific project in this file.
//
const { on } = require("events");
const express = require("express");
const port = 3000;

let app = express();
let routes = require("express").Router();
app.use(express.json())

// Global mock objects
const mockUserData =     {
  _id: "u1",
  username: "Creator",
  email: "creator@email.com",
  password: "$2b$10$gV2uOT3FJ.SLjJy7sQCXguMZpmTiXNFMZE5vE7FBaRjH6PJhu9gX6",
  birthdate: "2000-12-30T00:00:00.000Z",
  role: "creator",
  games: [],
  lists: [],
  __v: 0,
  token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MWFhNmRiYjNhNzZjNDhjYjM1OGQxNTciLCJpYXQiOjE2Mzg2Mzk2NTV9.9ybnhvuTaKzWODbmjvww3CSWBaz4gEQ_FUEPOOTydeA"
}

let mockGames = [
  {
    _id: "g1",
    name: "Rocket League",
    price: 0,
    short_description: "Voetbal en autorijden worden opnieuw gecombineerd in dit langverwachte, op fysica gebaseerde vervolg van de geliefde arena-klassieker Supersonic Acrobatic Rocket-Powered Battle-Cars!",
    long_description: "Voetbal en autorijden worden opnieuw gecombineerd in dit langverwachte, op fysica gebaseerde vervolg van de geliefde arena-klassieker Supersonic Acrobatic Rocket-Powered Battle-Cars!\n\nHet futuristische sport-actie spel Rocket League® voorziet spelers van voertuigen met boosters waarmee je tegen ballen aan kunt crashen om zo geweldige doelpunten of epische reddingen te maken in verschillende gedetailleerde arena's. Met behulp van een geavanceerd fysisch systeem om realistische acties te stimuleren, steunt Rocket League® op massa en momentum om spelers een volledig gevoel van intuïtieve controle te geven in deze ongelooflijke, krachtige geherinterpreteerde manier van verenigingsvoetbal spelen.",
    release_date: "2015-07-07T00:00:00.000Z",
    trailer_url: "https://www.youtube.com/embed/SgSX3gOrj60",
    reviews: [],
    category: "action",
    publisher: "Psyonix LLC",
    createdBy: {
        _id: "61aa6dbb3a76c48cb358d157",
        username: "Creator",
        email: "creator@email.com",
        password: "$2b$10$gV2uOT3FJ.SLjJy7sQCXguMZpmTiXNFMZE5vE7FBaRjH6PJhu9gX6",
        birthdate: "2000-12-30T00:00:00.000Z",
        role: "creator",
        games: [],
        lists: [],
        __v: 0
    },
    __v: 0
  },
  {
      _id: "g2",
      name: "Battlefield 2042",
      price: 59.99,
      short_description: "Battlefield™ 2042 is een first-person shooter waarin je terugkeert naar grootschalige oorlogsvoering. In een wereld uit de nabije toekomst die is getransformeerd door wanorde passen jij en je squad zich aan met een geavanceerd arsenaal om te winnen op dynamische en veranderende strijdvelden",
      long_description: "Battlefield™ 2042 is een first-person shooter waarin je terugkeert naar de iconische grootschalige oorlogsvoering van de franchise. Pas je aan en overwin obstakels in een wereld uit de nabije toekomst die is getransformeerd door wanorde. Vorm een squad en neem een geavanceerd arsenaal mee naar dynamische en veranderende strijdtonelen met 128 spelers, een ongekende schaal en epische verwoesting.\n\nDe volgende generatie van de populaire modes Conquest en Breakthrough bevat de grootste Battlefield-maps ooit en maximaal 128 spelers. Geniet van de intensiteit van totale oorlog op maps boordevol dynamisch weer en spectaculaire wereldgebeurtenissen.\n\nDe enorme, iconische sandbox-modus van Battlefield is terug van weggeweest: deze keer biedt de modus ondersteuning voor 128 spelers op de pc. De maps zijn specifiek ontworpen voor deze enorme schaal, met actie die is verdeeld in diverse soorten \"clusters\". De actie concentreert zich nu ook op sectors die uit meerdere vlaggen bestaan, in plaats van aparte controleposten.\n\nIn de terugkeer van Breakthrough vechten twee teams – aanvallers en verdedigers – om grootschaligere sectors terwijl de aanvallers oprukken naar het laatste missiedoel. Elke sector bevat een groter aantal spelers, waardoor je strategischere keuzes kunt maken en meer kansen krijgt om te flankeren. Nader de veroveringszones vanaf meerdere locaties en benut meerdere soorten tactische mogelijkheden.",
      release_date: "2021-11-19T00:00:00.000Z",
      trailer_url: "https://www.youtube.com/embed/ASzOzrB-a9E",
      reviews: [],
      category: "action",
      publisher: "Electronic Atrs",
      createdBy: {
          _id: "61aa6dbb3a76c48cb358d157",
          username: "Creator",
          email: "creator@email.com",
          password: "$2b$10$gV2uOT3FJ.SLjJy7sQCXguMZpmTiXNFMZE5vE7FBaRjH6PJhu9gX6",
          birthdate: "2000-12-30T00:00:00.000Z",
          role: "creator",
          games: [],
          lists: [],
          __v: 0
      },
      __v: 0
  },
];

const mockArticles = [
  {
      _id: "a1",
      user: {
          _id: "u1",
          username: "Creator",
          email: "creator@email.com",
          password: "$2b$10$gV2uOT3FJ.SLjJy7sQCXguMZpmTiXNFMZE5vE7FBaRjH6PJhu9gX6",
          birthdate: "2000-12-30T00:00:00.000Z",
          role: "creator",
          games: [],
          lists: [],
          __v: 0
      },
      title: "Article1",
      subtitle: "Subtitle Article1",
      content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus euismod et urna id viverra. Sed magna ex, fermentum eu mauris hendrerit, pharetra pretium neque. Sed feugiat mollis nisi vel consequat. Duis sodales interdum dolor, facilisis dignissim nibh vestibulum sed. Proin tincidunt ullamcorper sapien nec venenatis. Vestibulum gravida ante quam, ut fringilla ex aliquet et. Nulla tristique varius ipsum. Cras tincidunt, leo ac bibendum dignissim, felis arcu dignissim magna, non laoreet velit quam nec quam. Curabitur fermentum dui quis tempor auctor. Vivamus faucibus fringilla sapien a laoreet. Pellentesque dictum tincidunt ex non pulvinar. Fusce rhoncus molestie odio, a consectetur arcu suscipit quis. Nunc tellus neque, fringilla sed felis id, mollis maximus turpis.\n\nEtiam euismod efficitur justo, vitae semper tellus ornare eget. Etiam efficitur, ante eget vehicula efficitur, justo nibh condimentum nunc, ac tincidunt mauris libero et libero. Nulla luctus ac nulla quis venenatis. Aenean rutrum pretium massa sed maximus. Morbi mi velit, hendrerit id diam ut, vulputate elementum tellus. Vestibulum tincidunt vitae mauris eget semper. Pellentesque dapibus turpis vitae turpis semper fringilla. Curabitur placerat nisl massa, lacinia feugiat metus mollis ac.\n\nVestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed pulvinar magna sed ante posuere, sed mattis mauris eleifend. Phasellus vel elementum nisl, sit amet egestas elit. Donec vulputate condimentum erat, eu tempor metus tincidunt quis. Quisque dignissim orci id urna malesuada volutpat. Etiam sit amet dictum diam. Suspendisse potenti. Sed consectetur vehicula orci a auctor.\n\nEtiam pretium hendrerit sem in consequat. Nulla facilisi. Nunc tempus luctus consequat. Proin et pharetra quam, tempus blandit enim. Morbi eu justo id tortor tempor laoreet vehicula at sem. Morbi blandit vehicula lacus, et feugiat libero congue quis. In vel erat venenatis, semper justo ac, auctor ipsum.\n\nDonec dignissim pharetra sapien. In sapien leo, maximus nec dui id, pretium dignissim felis. Aliquam ut lorem at ante rhoncus semper vitae vel erat. Pellentesque eget lorem tellus. Proin pretium enim ornare sem porttitor, non euismod mauris sollicitudin. Praesent sed augue at risus feugiat dictum sed non massa. Pellentesque condimentum, diam eu lobortis suscipit, magna nunc luctus nisi, eu sodales risus ligula ut est. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Praesent quis congue orci, ac accumsan risus. Integer et purus pharetra, mattis nulla quis, commodo elit. Vivamus id metus eu nunc varius tempus. Pellentesque tempus turpis eu accumsan egestas. Vivamus sagittis eleifend pulvinar. Ut viverra diam eget quam porta pellentesque. Donec id magna eu leo efficitur placerat. Ut dictum velit at tincidunt interdum.",
      date: "2021-12-05T12:46:58.304Z",
      category: "news",
      __v: 0
  },
  {
    _id: "a2",
    user: {
        _id: "u1",
        username: "Creator",
        email: "creator@email.com",
        password: "$2b$10$gV2uOT3FJ.SLjJy7sQCXguMZpmTiXNFMZE5vE7FBaRjH6PJhu9gX6",
        birthdate: "2000-12-30T00:00:00.000Z",
        role: "creator",
        games: [],
        lists: [],
        __v: 0
    },
    title: "Article2",
    subtitle: "Subtitle Article2",
    content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus euismod et urna id viverra. Sed magna ex, fermentum eu mauris hendrerit, pharetra pretium neque. Sed feugiat mollis nisi vel consequat. Duis sodales interdum dolor, facilisis dignissim nibh vestibulum sed. Proin tincidunt ullamcorper sapien nec venenatis. Vestibulum gravida ante quam, ut fringilla ex aliquet et. Nulla tristique varius ipsum. Cras tincidunt, leo ac bibendum dignissim, felis arcu dignissim magna, non laoreet velit quam nec quam. Curabitur fermentum dui quis tempor auctor. Vivamus faucibus fringilla sapien a laoreet. Pellentesque dictum tincidunt ex non pulvinar. Fusce rhoncus molestie odio, a consectetur arcu suscipit quis. Nunc tellus neque, fringilla sed felis id, mollis maximus turpis.\n\nEtiam euismod efficitur justo, vitae semper tellus ornare eget. Etiam efficitur, ante eget vehicula efficitur, justo nibh condimentum nunc, ac tincidunt mauris libero et libero. Nulla luctus ac nulla quis venenatis. Aenean rutrum pretium massa sed maximus. Morbi mi velit, hendrerit id diam ut, vulputate elementum tellus. Vestibulum tincidunt vitae mauris eget semper. Pellentesque dapibus turpis vitae turpis semper fringilla. Curabitur placerat nisl massa, lacinia feugiat metus mollis ac.\n\nVestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed pulvinar magna sed ante posuere, sed mattis mauris eleifend. Phasellus vel elementum nisl, sit amet egestas elit. Donec vulputate condimentum erat, eu tempor metus tincidunt quis. Quisque dignissim orci id urna malesuada volutpat. Etiam sit amet dictum diam. Suspendisse potenti. Sed consectetur vehicula orci a auctor.\n\nEtiam pretium hendrerit sem in consequat. Nulla facilisi. Nunc tempus luctus consequat. Proin et pharetra quam, tempus blandit enim. Morbi eu justo id tortor tempor laoreet vehicula at sem. Morbi blandit vehicula lacus, et feugiat libero congue quis. In vel erat venenatis, semper justo ac, auctor ipsum.\n\nDonec dignissim pharetra sapien. In sapien leo, maximus nec dui id, pretium dignissim felis. Aliquam ut lorem at ante rhoncus semper vitae vel erat. Pellentesque eget lorem tellus. Proin pretium enim ornare sem porttitor, non euismod mauris sollicitudin. Praesent sed augue at risus feugiat dictum sed non massa. Pellentesque condimentum, diam eu lobortis suscipit, magna nunc luctus nisi, eu sodales risus ligula ut est. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Praesent quis congue orci, ac accumsan risus. Integer et purus pharetra, mattis nulla quis, commodo elit. Vivamus id metus eu nunc varius tempus. Pellentesque tempus turpis eu accumsan egestas. Vivamus sagittis eleifend pulvinar. Ut viverra diam eget quam porta pellentesque. Donec id magna eu leo efficitur placerat. Ut dictum velit at tincidunt interdum.",
    date: "2021-12-05T12:46:58.304Z",
    category: "news",
    __v: 0
}
]


// Add CORS headers so our external Angular app is allowed to connect
app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type,authorization"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});

routes.post("/api/login", (req, res, next) => {
  res.status(200).json(mockUserData);
});

routes.get("/api/game", (req, res, next) => {
  res.status(200).json(mockGames);
});

routes.get("/api/article", (req, res, next) => {
  res.status(200).json(mockArticles);
});

routes.get("/api/article/:id", (req, res, next) => {
  const article = mockArticles.filter(item => item._id == req.params.id)[0];
  res.status(200).json(article);
});

routes.put("/api/article/:id", (req, res, next) => {
  res.status(204).end()
});

routes.delete("/api/article/:id", (req, res, next) => {
  res.status(204).end()
});


app.use(routes);

app.use("*", function (req, res, next) {
  next({ error: "Non-existing endpoint" });
});

app.use((err, req, res, next) => {
  res.status(400).json(err);
});

app.listen(port, () => {
  console.log("Mock backend server running on port", port);
});

process.on("uncaughtException", (err) => {
  console.log("There was an uncaught error", err);
  process.exit(1); //mandatory (as per the Node.js docs)
});
