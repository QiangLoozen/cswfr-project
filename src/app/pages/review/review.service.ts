import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, tap, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Review } from './review.model';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {

  private readonly headers = new HttpHeaders({
    'Content-Type': 'application/json',
  });
  
  constructor(private http: HttpClient) { }

  public create(review: Review, gameId: string, options?: any): Observable<Object> {
    const endpoint = `${environment.API_URL}game/${gameId}/reviews`;
    console.log(`post ${endpoint}`);

    return this.http.post(endpoint, review, {...options, ...this.headers}).pipe(
      tap(console.log),
      catchError(this.handleError)
    )
  }
  public update(review: Review, gameId: string, options?: any): Observable<Object>  {
    const endpoint = `${environment.API_URL}game/${gameId}/reviews/${review._id}`;
    console.log(`put ${endpoint}`);

    return this.http.put(endpoint, review, {...options, ...this.headers}).pipe(
      tap(console.log),
      catchError(this.handleError)
    )
  }
  public delete(review: Review, gameId: string, options?: any): Observable<Object>  {
    const endpoint = `${environment.API_URL}game/${gameId}/reviews/${review._id?.toString()}`;
    console.log(`delete ${endpoint}`);

    return this.http.delete(endpoint, {...options, body: review}).pipe(
      tap(console.log),
      catchError(this.handleError)
    )
  }

  public addLike(gameId: string, reviewId: string, userId: string): Observable<Object> {
    const endpoint = `${environment.API_URL}game/${gameId}/reviews/${reviewId}/addLike`;
    console.log(`post ${endpoint}`);

    return this.http.post(endpoint, {userId: userId}, {headers: this.headers}).pipe(
      tap(console.log),
      catchError(this.handleError)
    )
  }

  public removeLike(gameId: string, reviewId: string, userId: string): Observable<Object> {
    const endpoint = `${environment.API_URL}game/${gameId}/reviews/${reviewId}/removeLike`;
    console.log(`delete ${endpoint}`);

    return this.http.post(endpoint, {userId: userId}, {headers: this.headers}).pipe(
      tap(console.log),
      catchError(this.handleError)
    )
  }


  public handleError(error: HttpErrorResponse): Observable<any> {
    console.log(error);

    // const errorResponse: Alert = {
    //   type: 'error',
    //   message: error.error.message || error.message,
    // };
    const errorResponse = {
        type: 'error',
        message: error.error.message || error.message
    }
    // return an error observable with a user-facing error message
    return throwError(errorResponse);
  }
}
