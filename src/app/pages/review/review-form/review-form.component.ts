import { HttpHeaders } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { of, Subscription, switchMap } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { Game } from '../../game/game.model';
import { GameService } from '../../game/game.service';
import { User } from '../../user/user.model';
import { Review } from '../review.model';
import { ReviewService } from '../review.service';

@Component({
  selector: 'app-review-form',
  templateUrl: './review-form.component.html',
  styleUrls: ['./review-form.component.css']
})
export class ReviewFormComponent implements OnInit, OnDestroy {

  game?: Game;
  review?: Review;
  reviewId?: String;
  currentuser?: User;
  submitted = false;
  title!: String;
  options?: Object;


  subscription?: Subscription;

  reviewForm = this.formBuilder.group({
    _id: [''],
    title: ['', Validators.required],
    content: ['', Validators.required],
    rating: ['', [Validators.required, Validators.min(0), Validators.max(5)]],
    likes: [''],
    date: [''],
    user: ['']
  })

  get f() {return this.reviewForm.controls;}


  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router,
    private gameService: GameService,
    private reviewService: ReviewService,
    private authService: AuthService
  ) { }


  ngOnInit(): void {
    this.title = 'create'

    this.authService.currentUser$.pipe( // GET CURRENT USER FOR AUTHORISATION
      switchMap(cu => {
        this.currentuser = cu;
        // Add currentusertoken to httpheader
        console.log("current user: ", this.currentuser)

        this.options = {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + this.currentuser?.token
          })
        } 
        return this.route.paramMap
      }),
      switchMap((params: ParamMap) => { // GET GAME ID / REVIEW ID
        const gameId = params.get('gameId');
        const reviewId = params.get('reviewId');
        if(reviewId) {this.reviewId = reviewId};

        if(gameId) {
          return this.gameService.read(gameId);
        } else {
          return of(undefined);
        }
      })
    )
    .subscribe(gameDetails => { // GET GAME DETAILS AND REVIEW
      this.game = gameDetails;

      if(this.reviewId) {
        console.log("review id: " + this.reviewId)
        this.review = this.game?.reviews.find(review => review._id?.toString() == this.reviewId)
        if(this.review) {
          console.log("found review: ", this.review)
          this.reviewForm.patchValue(this.review)
          this.title = "Update review"
        };
      }

      console.log("Header: ", this.options)
    })
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

  onSubmit() {
    this.submitted = true;
    if(this.reviewForm.invalid) return;
    if(!this.review) {
      // Create review
      let newReview = this.reviewForm.value;
      delete newReview._id;
      newReview.date = new Date();
      newReview.likes = [];
      newReview.user= this.currentuser;
      console.log("Review to be created:", newReview)
      console.log("Sending header: ", this.options)
      this.reviewService.create(newReview, this.game!._id!.toString(), this.options).subscribe(() => {
        this.router.navigate(['../..'], {relativeTo: this.route});
      })
    } else {
      // Update review
      console.log("Updated review:", this.reviewForm.value)
      console.log("Sending header: ", this.options)
      this.reviewService.update(this.reviewForm.value, this.game!._id!.toString(), this.options).subscribe(() => {
        this.router.navigate(['../../..'], {relativeTo: this.route});
      })
    }
  }

  onDelete() {
    if(this.review && this.game) {
      this.reviewService.delete(this.review, this.game._id!.toString(), this.options).subscribe(() => {
        this.router.navigate(['/games/', this.game?._id]);
      })
    } else {
      console.error("Missing information for deleting review")
    }
  }
}
