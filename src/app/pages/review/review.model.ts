import { Entity } from "src/app/shared/entity.model";
import { User } from "../user/user.model";

export class Review extends Entity {
    user?: User;
    title: string = '';
    content: string = '';
    rating: number = 0;
    likes: string[] = [];
    date: Date = new Date();
}