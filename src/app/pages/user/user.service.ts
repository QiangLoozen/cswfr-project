import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, tap } from 'rxjs';
import { EntityService } from 'src/app/shared/entity.service';
import { environment } from 'src/environments/environment';
import { User } from './user.model';

@Injectable({
  providedIn: 'root'
})

export class UserService extends EntityService<User> {

  private readonly headers = new HttpHeaders({
    'Content-Type': 'application/json',
  });

  constructor(http: HttpClient) { 
    super(http, environment.API_URL, 'user');
  }

  public getAllEmails(options?: any): Observable<string[]> {
    const endpoint = `${environment.API_URL}user/emails`;
    console.log(`get ${endpoint}`);

    return this.http.get(endpoint, {...options}).pipe(
      tap(console.log),
      catchError(this.handleError)
    )
  }

  public getFriends(userId: string, options?: any): Observable<User[]> {
    const endpoint = `${environment.API_URL}user/${userId}/friends`;
    console.log(`get ${endpoint}`);

    return this.http.get(endpoint, {...options}).pipe(
      tap(console.log),
      catchError(this.handleError)
    )
  }

  public getRecommendedFriends(userId: string, options?: any): Observable<User[]> {
    const endpoint = `${environment.API_URL}user/${userId}/recommendations/recommendedFriends`;
    console.log(`get ${endpoint}`);

    return this.http.get(endpoint, {...options}).pipe(
      tap(console.log),
      catchError(this.handleError)
    )
  }

  public getFriendRequests(userId: string, options?: any): Observable<User[]> {
    const endpoint = `${environment.API_URL}user/${userId}/friendRequests`;
    console.log(`get ${endpoint}`);

    return this.http.get(endpoint, {...options}).pipe(
      tap(console.log),
      catchError(this.handleError)
    )
  }

  public friendRequest(senderId: string, userId: string, options?: any): Observable<Object> {
    const endpoint = `${environment.API_URL}user/${userId}/friendRequest`;
    const body = {senderId: senderId};
    console.log(`post ${endpoint}`);

    return this.http.post(endpoint, body , {...options, ...this.headers}).pipe(
      tap(console.log),
      catchError(this.handleError)
    )
  }

  public friendRequestAccept(senderId: string, userId: string, options?: any): Observable<Object> {
    const endpoint = `${environment.API_URL}user/${userId}/friendRequest/accept`;
    const body = {senderId: senderId};
    console.log(`post ${endpoint}`);

    return this.http.post(endpoint, body , {...options, ...this.headers}).pipe(
      tap(console.log),
      catchError(this.handleError)
    )
  }

  public friendRequestDecline(senderId: string, userId: string, options?: any): Observable<Object> {
    const endpoint = `${environment.API_URL}user/${userId}/friendRequest/decline`;
    const body = {senderId: senderId};
    console.log(`post ${endpoint}`);

    return this.http.post(endpoint, body , {...options, ...this.headers}).pipe(
      tap(console.log),
      catchError(this.handleError)
    )
  }

  public friendRemove(senderId: string, userId: string, options?: any): Observable<Object> {
    const endpoint = `${environment.API_URL}user/${userId}/removeFriend`;
    const body = {senderId: senderId};
    console.log(`post ${endpoint}`);

    return this.http.post(endpoint, body ,{...options, ...this.headers}).pipe(
      tap(console.log),
      catchError(this.handleError)
    )
  }
}
