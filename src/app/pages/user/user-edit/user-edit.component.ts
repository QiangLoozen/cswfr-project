import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { User } from 'src/app/pages/user/user.model';
import { UserService } from 'src/app/pages/user/user.service';
import { Subscription, switchMap } from 'rxjs';
import { futureDateValidator } from '../user.validation';
import { AuthService } from 'src/app/auth/auth.service';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit, OnDestroy{

  userId = 0;
  user: User | null = null;
  submitted = false;
  userEmails?: string[];
  options?: Object;

  userForm = this.formBuilder.group({
    _id: [''],
    username: ['', Validators.required],
    email: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
    role: [''],
    birthdate: ['', [Validators.required, futureDateValidator()]],
    profilePicture: [''],
    token: [''],
    games: [''],
    lists: ['']
  }) 

  get f() {return this.userForm.controls;}
  subscription?: Subscription;

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.getUserInfo();
    this.getAllEmails();
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

  onSubmit() {
    this.submitted = true;

    if(this.userForm.valid) {
      let updatedUser = this.userForm.value;
      let updatedEmail:string = this.userForm.value.email;
      const formattedEmail = updatedEmail.trim().toLowerCase();

      let emailExist = false;
      this.userEmails?.forEach(email => {
        if(formattedEmail === email && formattedEmail !== this.user?.email) {
          this.userForm.controls.email.setErrors({alreadyExist: true})
          console.warn(`Email: ${formattedEmail} already exist!`)
          emailExist = true;
        }
      });
      if(!emailExist) {
        
        updatedUser.email = formattedEmail;
        console.log("Updated", updatedUser)
        this.userService.update(updatedUser, this.options).subscribe(() => {
          this.authService.currentUser$.next(updatedUser);
          localStorage.setItem('currentuser', JSON.stringify(updatedUser));
          this.router.navigate(['users', this.user?._id]);
        });
      }
    }
  }

  onDelete() {
    if(this.user?._id) {
      this.userService.delete(this.user?._id.toString(), this.options).subscribe(() => {
        this.authService.currentUser$.next(undefined!);
        this.router.navigate(['/']);
      });
    }
  }

  private getUserInfo() {
    this.subscription = this.authService.currentUser$.subscribe(usr => {
      this.user = usr;
      this.options = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + this.user?.token
        })
      } 
      this.route.paramMap.subscribe(res => {
        const id = res.get('userId');
        if(!id || id != this.user?._id) {
          console.log("user may not edit")
          this.router.navigate(['/'])
        } else {
          console.log("user may edit")
        }
      })
      let date = this.user.birthdate.toString().split('T')[0]

      this.userForm.patchValue(this.user);
      this.userForm.patchValue({birthdate: date});
    });
  }
  
  private getAllEmails() {
    this.userService.getAllEmails().subscribe((r) => {
      this.userEmails = r
    })
  }

}
