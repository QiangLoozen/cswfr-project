import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable, switchMap } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { ListService } from '../../list.service';
import { List, User } from '../../user.model';
import { UserService } from '../../user.service';


@Component({
  selector: 'app-user-lists-details',
  templateUrl: './user-lists-details.component.html',
  styleUrls: ['./user-lists-details.component.css']
})
export class UserListsDetailsComponent implements OnInit {

  list: List | null = null;
  userDetails: User | null = null;
  options?: Object;
  currentuser: User | null = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private listService: ListService,
    public authService: AuthService
  ) { }

  ngOnInit(): void {
    this.authService.currentUser$.subscribe(cu => {
      this.currentuser = cu;
      this.options = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + this.currentuser?.token
        })
      } 
      this.getUser()
    });
  }

  onRemoveGame(gameId?: any) {
    this.listService.removeFromList(this.userDetails!._id!.toString(), this.list!._id!.toString(), gameId!.toString(), this.options).subscribe(() => {
      this.getUser();
    })
  }

  private getUser() {
    this.route.parent!.paramMap
      .pipe(
        switchMap((params: ParamMap) => {
          let id = params.get('userId');
          return this.userService.read(id ?? 0, this.options)
          }
        )
      )
      .subscribe(r => {
        this.userDetails = r

        this.getList();
      })
  }

  private getList() {
    this.route.paramMap
      .subscribe((params: ParamMap) => {
        const listId = params.get('listId');
        if(listId) {
          this.userDetails?.lists.forEach((list) => {
            if(list._id?.toString() === listId) {
              this.list = list;
            }
          });
        } else {
          console.error("could not found the listparam")
        }
      })
  }
}
