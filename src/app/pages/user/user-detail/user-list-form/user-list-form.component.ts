import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { of, switchMap } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { ListService } from '../../list.service';
import { List, User } from '../../user.model';
import { UserService } from '../../user.service';

@Component({
  selector: 'app-user-list-form',
  templateUrl: './user-list-form.component.html',
  styleUrls: ['./user-list-form.component.css']
})
export class UserListFormComponent implements OnInit {
  title!: String;
  submitted = false;
  list: List | null = null;
  userLists: List[] | null = null;
  userId?: string;
  get f() {return this.listForm.controls;}


  listForm = this.formBuilder.group({
    _id: [''],
    name: ['', Validators.required], 
    description: ['', Validators.required],
    date: [''],
    games: [[]]
  })

  options?: Object;
  currentuser: User | null = null;
  
  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router,
    private listService: ListService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.authService.currentUser$.subscribe(cu => {
      this.currentuser = cu;
      this.options = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + this.currentuser?.token
        })
      } 
      this.getUserLists()
    });
  }

  onSubmit() {
    this.submitted = true;
    if(this.listForm.invalid) {
      console.log("Invalid form values")
      return;
    }
    if(this.list) {
      // Update list
      let updatedList = this.listForm.value
      const listId = updatedList._id;
      this.listService.updateList(this.userId!, listId, this.listForm.value, this.options).subscribe(() => {
        this.router.navigate(['../../', listId], {relativeTo: this.route});
      })
    } else {
      // Create new list
      this.listForm.patchValue({
        date: new Date()
      })

      let newList = this.listForm.value
      delete newList._id;

      this.listService.createList(this.userId!, newList, this.options).subscribe(() => {
        this.router.navigate(['..'], {relativeTo: this.route});
      })
    }
  }

  onDelete() {
    if(this.list){
      this.listService.deleteList(this.userId!, this.list._id!.toString(), this.options).subscribe(() => {
        this.router.navigate(['../..'], {relativeTo: this.route});
      })
    } else {
      console.error("Could not find the list")
    }
  }

  private getUserLists() {
    this.route.parent!.paramMap
    .pipe(
      switchMap((params: ParamMap) => {
        let id = params.get('userId');
        if(id) {
          this.userId = id;
          return this.listService.getLists(id!, this.options);
        } else {
          return of(null)
        }
      })
    )
    .subscribe((r) => {
      this.userLists = r;
      this.getListInfo();
    })
  }

  private getListInfo() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      const listId = params.get('listId');
      if(listId) {
        console.log("search: " + listId)
        this.title = 'Edit list'
        this.userLists?.forEach(list => {
          console.log(list._id?.toString()  + " and " + listId)
          if(list._id?.toString() == listId) {
            this.list = list;
            console.log(list)
          }
      });
      if(this.list) {
        this.listForm.patchValue(this.list);
      }else {
        this.title = 'Create new list'
      }

      } else {
        this.title = 'Create new list'
      }
    });
  }

}
