import { HttpHeaders } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable, Subscription, switchMap } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { AlertService } from 'src/app/shared/alert/alert.service';
import { User } from '../../user.model';
import { UserService } from '../../user.service';

@Component({
  selector: 'app-user-friends',
  templateUrl: './user-friends.component.html',
  styleUrls: ['./user-friends.component.css']
})
export class UserFriendsComponent implements OnInit, OnDestroy {

  user$?: Observable<User>
  userDetails: User | null = null;  
  currentuser: User | null = null;
  userdetailSubscription?: Subscription;
  friends$?: Observable<User[]>;
  friends?: User[];
  requests$?: Observable<User[]>;
  requests?: User[]
  options?: Object;

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.authService.currentUser$.subscribe(cu => {
      this.currentuser = cu;
      this.options = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + this.currentuser?.token
        })

      }

      this.user$ = this.route.parent!.paramMap
      .pipe(
        switchMap((params: ParamMap) => {
          let id = params.get('userId');
          return this.userService.read(id ?? 0, this.options)
          }
        )
      );
      this.user$.subscribe((r) => {
        this.userDetails = r;
        this.friends$ = this.userService.getFriends(r!._id!.toString(), this.options)
        this.friends$?.subscribe((usrs) => {
          this.friends = usrs;
        });
  
        this.requests$ = this.userService.getFriendRequests(r!._id!.toString(), this.options)
        this.requests$.subscribe((reqs) => {
          this.requests = reqs;
        })
      })
    })
  }

  onFriendRequestAccept(senderId: string | number | undefined) {
    if(!this.userDetails || !senderId) {
      console.error("currentuser or userdetails not available")
    }
    this.userService.friendRequestAccept(senderId!.toString(), this.userDetails!._id!.toString(), this.options)
      .pipe(
        switchMap(() => {
          return this.user$!
        })
      )
      .subscribe((r) => {
        this.userDetails = r;
        this.friends$?.subscribe((usrs) => {
          this.friends = usrs;
        });
        this.requests$?.subscribe((reqs) => {
          this.requests = reqs;
        })
      });
  }

  onFriendRequestDecline(senderId: string | number | undefined) {
    if(!this.userDetails || !senderId) {
      console.error("currentuser or userdetails not available")
    }
    this.userService.friendRequestDecline(senderId!.toString(), this.userDetails!._id!.toString(), this.options)
      .pipe(
        switchMap(() => {
          return this.user$!
        })
      )
      .subscribe((r) => {
        this.userDetails = r;
        this.requests$?.subscribe((reqs) => {
          this.requests = reqs;
        })
      });
  }

  onRemoveFriend(senderId: string | number | undefined) {
    if(!this.userDetails || !senderId) {
      console.error("currentuser or userdetails not available")
    }

    this.userService.friendRemove(senderId!.toString(), this.userDetails!._id!.toString(), this.options)
      .pipe(
        switchMap(() => {
          return this.user$!
        })
      )
      .subscribe((r) => {
        this.userDetails = r;
        this.friends$?.subscribe((usrs) => {
          this.friends = usrs;
        });
      });
  }

  ngOnDestroy(): void {
    this.userdetailSubscription?.unsubscribe();
  }

}
