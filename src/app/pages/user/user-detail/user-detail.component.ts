import { HttpHeaders } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable, of, Subscription, switchMap } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { User } from 'src/app/pages/user/user.model';
import { UserService } from 'src/app/pages/user/user.service';
import { AlertService } from 'src/app/shared/alert/alert.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit, OnDestroy {

  user: User | null = null;
  user$!: Observable<User>;
  currentUser: User | null = null;
  active = "games";
  pendingRequest: boolean = false;
  isFriend: boolean = false;
  friends?: User[];
  requests?: User[];
  currentUserRequests?: User[];
  options?: Object;
  
  userdetailSubscription!: Subscription;
  friendRequestSubscription!: Subscription;

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private authService: AuthService,
    private alertService: AlertService) {}

  ngOnInit(): void {
    this.getUserDetails();
  }

  private getUserDetails() {
    this.userdetailSubscription = this.authService.currentUser$.pipe(
      switchMap(cu => {
        this.currentUser = cu;
        this.options = {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + this.currentUser?.token
          })
        } 
        console.log("added token")
        return this.userService.getFriendRequests(this.currentUser!._id!.toString(), this.options);
      }),
      switchMap(currentuserRequests => {
        this.currentUserRequests = currentuserRequests;
        return this.route.paramMap
      }),
      switchMap((params: ParamMap) => {
        // Getting user details
        let id = params.get('userId');
        console.log("retrieving")
        return this.user$ = this.userService.read(id ?? 0, this.options)
      }),
      switchMap(user => {
        // Getting friendrequest for checking relation with others
        this.user = user;
        if(user) {
          return this.userService.getFriendRequests(this.user._id!.toString(), this.options);
        } else {
          return of(undefined);
        } 
      }),
      switchMap(reqs => {
        // Getting friendrequest
        this.requests = reqs;
        return this.userService.getFriends(this.user!._id!.toString(), this.options)
      })
    ).subscribe(f => {
      this.friends = f;
      this.checkRelation();
    }, err => this.alertService.error("Server is currently offline, please try again later."));
  }

  onFriendRequest() {
    this.friendRequestSubscription = this.userService.friendRequest(this.currentUser!._id!.toString(), this.user!._id!.toString(), this.options)
    .pipe(
      switchMap(() => {
        return this.user$;
      })
    )
    .subscribe((r) => {
      this.user = r;
      this.getUserDetails();
      this.friendRequestSubscription.unsubscribe();
    })
  }

  private checkRelation(): void {
    this.currentUserRequests!.forEach(element => {
      if(element._id?.toString() == this.user?._id) {
        this.pendingRequest = true;
        console.log("pending request")
        return;
      }
    });

    this.requests!.forEach(element => {
      if(element._id?.toString() == this.currentUser?._id) {
        this.pendingRequest = true;
        console.log("pending request")
        return;
      }
    });
    this.friends?.forEach(element => {
      if(element._id?.toString() == this.currentUser?._id) {
        this.isFriend = true;
        console.log("current user is friend")
        return;
      }
    });
  }

  ngOnDestroy(): void {
    this.userdetailSubscription?.unsubscribe();
  }

}
