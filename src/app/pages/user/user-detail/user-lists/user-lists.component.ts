import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable, switchMap } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { User } from '../../user.model';
import { UserService } from '../../user.service';

@Component({
  selector: 'app-user-lists',
  templateUrl: './user-lists.component.html',
  styleUrls: ['./user-lists.component.css']
})
export class UserListsComponent implements OnInit {
  
  user$: Observable<User> | null = null;
  userDetails: User | null = null;
  options?: Object;
  currentuser: User | null = null;

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    public authService: AuthService
  ) { }

  ngOnInit(): void {
    this.authService.currentUser$.subscribe(cu => {
      this.currentuser = cu;
      this.options = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + this.currentuser?.token
        })
      }
      
      this.user$ = this.route.parent!.paramMap
        .pipe(
          switchMap((params: ParamMap) => {
            let id = params.get('userId');
            return this.userService.read(id ?? 0, this.options)
            }
          )
        );

      this.user$.subscribe((r) => {
        this.userDetails = r;
      })

    });
    
  }

}
