import { HttpHeaders } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { of, Subscription, switchMap } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { User } from '../../user.model';
import { UserService } from '../../user.service';

@Component({
  selector: 'app-user-games',
  templateUrl: './user-games.component.html',
  styleUrls: ['./user-games.component.css']
})
export class UserGamesComponent implements OnInit, OnDestroy {

  userDetails?: User;
  userSubscription?: Subscription;
  options?: Object;
  currentuser: User | null = null;

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.getUserDetails();
  }

  getUserDetails() {
    this.authService.currentUser$.subscribe(cu => {
      this.currentuser = cu;
      this.options = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + this.currentuser?.token
        })
      } 

      this.userSubscription = this.route.parent!.paramMap
      .pipe(
        switchMap((params: ParamMap) => {
          const userId = params.get('userId');
          if(userId) {
            return this.userService.read(userId, this.options);
          } else {
            return of(undefined);
          }
        })
      )
      .subscribe((r) => {
        this.userDetails = r;
      })
    });
  }

  ngOnDestroy(): void {
    this.userSubscription?.unsubscribe();
  }
}
