import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { createHostListener } from '@angular/compiler/src/core';
import { Injectable } from '@angular/core';
import { catchError, Observable, tap, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { List } from './user.model';

@Injectable({
  providedIn: 'root'
})
export class ListService {

  private readonly headers = new HttpHeaders({
    'Content-Type': 'application/json',
  });
  
  constructor(private http: HttpClient) { }

  public getLists(userId: string, options?: any): Observable<List[]> {
    const endpoint = `${environment.API_URL}user/${userId}/lists`;
    console.log(`get ${endpoint}`);

    return this.http.get(endpoint, {...options}).pipe(
      tap(console.log),
      catchError(this.handleError)
    )
  }
  public createList(userId: string, list: List, options?: any): Observable<Object> {
    const endpoint = `${environment.API_URL}user/${userId}/lists`;
    console.log(`post ${endpoint}`);

    return this.http.post(endpoint, list, {...options, ...this.headers}).pipe(
      tap(console.log),
      catchError(this.handleError)
    )
  }
  public updateList(userId: string, listId: string, list: List, options?: any): Observable<Object> {
    const endpoint = `${environment.API_URL}user/${userId}/lists/${listId}`;
    console.log(`put ${endpoint}`);

    return this.http.put(endpoint, list, {...options, ...this.headers}).pipe(
      tap(console.log),
      catchError(this.handleError)
    )
  }
  public deleteList(userId: string, listId: string, options?: any): Observable<Object> {
    const endpoint = `${environment.API_URL}user/${userId}/lists/${listId}`;
    console.log(`delete ${endpoint}`);

    return this.http.delete(endpoint, {...options}).pipe(
      tap(console.log),
      catchError(this.handleError)
    )
  }
  public addToList(userId: string, listId: string, gameId: string, options?: any): Observable<Object> {
    const endpoint = `${environment.API_URL}user/${userId}/lists/${listId}/addGame/${gameId}`;
    console.log(`post  ${endpoint}`);

    return this.http.post(endpoint, null, {...options}).pipe(
      tap(console.log),
      catchError(this.handleError)
    )
  }
  public removeFromList(userId: string, listId: string, gameId: string, options?: any): Observable<Object> {
    const endpoint = `${environment.API_URL}user/${userId}/lists/${listId}/removeGame/${gameId}`;
    console.log(`delete  ${endpoint}`);

    return this.http.delete(endpoint, {...options}).pipe(
      tap(console.log),
      catchError(this.handleError)
    )
  }

  public handleError(error: HttpErrorResponse): Observable<any> {
    console.log(error);

    // const errorResponse: Alert = {
    //   type: 'error',
    //   message: error.error.message || error.message,
    // };
    const errorResponse = {
        type: 'error',
        message: error.error.message || error.message
    }
    // return an error observable with a user-facing error message
    return throwError(errorResponse);
  }
}

