import { AbstractControl, FormGroup, ValidationErrors, ValidatorFn } from "@angular/forms";



export  function confirmPassword(controlName: string, secondControlName: string){
    return(formGroup: FormGroup) => {
        const password = formGroup.controls[controlName];
        const confirm_password = formGroup.controls[secondControlName];

        if(confirm_password.errors && !confirm_password.errors.matching) {
            return;
        }

        if(password.value !== confirm_password.value) {
            confirm_password.setErrors({incorrectConfirm: true})
        } else {
            confirm_password.setErrors(null);
        }
    }
}

export function futureDateValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
        const date = new Date(control.value);
        console.log(`validate: ${date}`)
        console.log(`today: ${new Date()}`)

        if (!date) {
            return null;
        }
        console.log(date > new Date() ? {FutureDate:true} : null)
        return date > new Date() ? {futureDate:true} : null
    }
}