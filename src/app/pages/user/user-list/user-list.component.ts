import { HttpHeaders } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { of, Subscription, switchMap } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { User } from 'src/app/pages/user/user.model';
import { UserService } from 'src/app/pages/user/user.service';
import { AlertService } from 'src/app/shared/alert/alert.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit, OnDestroy{

  users: User[] | null = null;
  recommendedFriends: User[] | null =  null;

  userSubscription?: Subscription;
  currentuser: User| null = null;
  constructor(
    private userService: UserService,
    private alertService: AlertService,
    private authService: AuthService
  ) {}

  options?: Object;

  ngOnInit(): void {

    
    this.authService.currentUser$
      .pipe(
        switchMap(cu => {
          if(cu?._id) {
            this.currentuser = cu;
            this.options = {
              headers: new HttpHeaders({
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.currentuser?.token
              })
            } 
            return this.userService.getRecommendedFriends(cu._id.toString());
          } else {
            return of(null);
          }
        })
      )
      .subscribe(res => {
        this.recommendedFriends = res;
        console.log(this.options)
        this.userService.list(this.options)
          .subscribe(
            users => this.users = users,
            err => this.alertService.error("Server is currently offline, please try again later.")
          );
      })

    
  }

  // Search filter
  onSearch(event: any) {
    let items = document.querySelectorAll(".table-row")
    const term = event.target.value.toLowerCase();
    Array.from(items).forEach((row) => {
        let contains = false;
        Array.from(row.querySelectorAll("td")).forEach((column) => {
            let detailText = column.innerText.toLowerCase();
            if (detailText.indexOf(term) != -1) {
                contains = true;
            }
        })
        if (contains) {
          row.setAttribute("class", "table-row");
        } else {
          row.setAttribute("class", "table-row d-none");
        }
    })  
  }

  ngOnDestroy(): void {
    this.userSubscription?.unsubscribe();
  }
}
