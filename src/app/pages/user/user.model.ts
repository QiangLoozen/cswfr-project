import { Entity } from "src/app/shared/entity.model";
import { Game } from "../game/game.model";

export enum Role {
    customer = "customer",
    creator = "creator"
}
export class User extends Entity{
    username: string = '';
    email: string = '';
    birthdate: Date = new Date();
    role: Role = Role.customer;
    password: string = '';
    token: string = '';
    lists: List[] = [];
    games: Game[] = [];
    profilePicture: string = '';
}

export class List extends Entity {
    name: string = '';
    description: string = '';
    date: Date = new Date();
    games: Game[] = []
}