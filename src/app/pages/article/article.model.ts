import { Entity } from "src/app/shared/entity.model";
import { User } from "../user/user.model";

export enum Category {
    news = "news"
}

export class Article extends Entity {
    title: String = '';
    subtitle: String = '';
    content: String = '';
    date?: Date;
    user?: User;
    category: Category = Category.news;
    banner: String = '';
}