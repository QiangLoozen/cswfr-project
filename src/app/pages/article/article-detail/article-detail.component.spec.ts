import { ComponentFixture, TestBed } from '@angular/core/testing';


import { Directive, HostListener, Input } from '@angular/core';
import { Article, Category } from '../article.model';
import { ArticleService } from '../article.service';
import { of, Subscription } from 'rxjs';
import { ArticleDetailComponent } from './article-detail.component';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { Role } from '../../user/user.model';
import { AuthService } from 'src/app/auth/auth.service';


//Mocking the routeLink directive
@Directive({
  selector: '[routerLink]'
})
export class RouterLinkStubDirective {
  @Input('routerLink') linkParams: any;
  navigatedTo: any = null;

  @HostListener('click')
  onClick(): void {
    this.navigatedTo = this.linkParams;
  }
}

//Mocking the response from the articleservice
const article: Article =
  {
    _id: "1",
    user: {
      _id: "123",
      token:"mytoken",
      birthdate: new Date(),
      email: "email@email.com",
      games: [],
      lists: [], 
      password: "password", 
      role: Role.creator,
      username: "Creator",
      profilePicture: ""
    },
    title: "testarticle",
    subtitle: "this is a subtitle",
    content: "this is the content",
    date: new Date(),
    category: Category.news,
    banner: ""
  }

describe('ArticleDetailComponent', () => {
  let component: ArticleDetailComponent;
  let fixture: ComponentFixture<ArticleDetailComponent>;
  
  //Mock objects
  let articleServiceSpy: any;
  let routerSpy: any;
  let authServiceSpy: any;
  beforeEach(() => {
    articleServiceSpy = jasmine.createSpyObj('ArticleService', ['read']);
    authServiceSpy = jasmine.createSpyObj('AuthService', [
      'login',
      'register',
      'logout',
      'getUserFromLocalStorage',
      'saveUserToLocalStorage',
      'userMayEdit',
    ]);
    TestBed.configureTestingModule({
      declarations: [
        ArticleDetailComponent,
        RouterLinkStubDirective
      ],
      imports: [],
      providers: [
        {provide: ArticleService, useValue: articleServiceSpy},
        {provide: AuthService, useValue: authServiceSpy},
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(
              convertToParamMap({
                id: "1"
              })
            )
          }
        }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(ArticleDetailComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => {
    fixture.destroy();
  });
  
  it('Should load article details', (done: DoneFn) => {
    articleServiceSpy.read.and.returnValue(of(article))
    fixture.detectChanges();

    expect(component.article).toEqual(article);
    expect(component).toBeTruthy();
    done()
  });
});
