import { HttpClient } from '@angular/common/http';
import { not } from '@angular/compiler/src/output/output_ast';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { Role } from '../user/user.model';
import { Article, Category } from './article.model';

import { ArticleService } from './article.service';

const articles: Article[] = [
  {
    _id: "1",
    user: {
      _id: "123",
      token:"mytoken",
      birthdate: new Date(),
      email: "email@email.com",
      games: [],
      lists: [], 
      password: "password", 
      role: Role.creator,
      username: "Creator",
      profilePicture: ""
    },
    title: "testarticle",
    subtitle: "this is a subtitle",
    content: "this is the content",
    date: new Date(),
    category: Category.news,
    banner: ""
  }
]

describe('ArticleService', () => {
  let service: ArticleService;
  let httpSpy : jasmine.SpyObj<HttpClient>;
  
  beforeEach(() => {
    httpSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);

    TestBed.configureTestingModule({
      providers: [{provide: HttpClient, useValue: httpSpy}]
    });
    service = TestBed.inject(ArticleService);
    httpSpy = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
  });

  it('Should return a list of articles', (done: DoneFn) => {
    httpSpy.get.and.returnValue(of(articles));
    service.list().subscribe((result: Article[] | null) => {
      expect(result!.length).toBe(1);
      expect(result![0]._id).toEqual(articles[0]._id);
      expect(service).toBeTruthy();
      expect(httpSpy.get).toHaveBeenCalled();

      done();
    });
  });

  it('Should create a new article', (done: DoneFn) => {
    httpSpy.post.and.returnValue(of(articles[0]));
    service.create(articles[0]).subscribe((result) => {
      expect(result.title).toBe(articles[0].title);
      expect(result.subtitle).toBe(articles[0].subtitle);
      expect(result.content).toBe(articles[0].content);
      expect(service).toBeTruthy();
      expect(httpSpy.post).toHaveBeenCalled();

      done();
    });
  });

  it('Should update an article', (done: DoneFn) => {
    // Unittest wordt (ook) in e2e afgehandeld, aangezien mijn API 204 no content teruggeeft
    let changed = articles[0];
    changed.title = "changedtitle"
    changed.content = "changedcontent";
    httpSpy.put.and.returnValue(of(null));
    service.update(changed).subscribe((result) => {
      expect(result).toEqual(null!)
      expect(httpSpy.put).toHaveBeenCalled();
      done();
    });
  });

  it('Should delete an article', (done: DoneFn) => {
    // Unittest wordt (ook) in e2e afgehandeld, aangezien mijn API 204 no content teruggeeft
    httpSpy.delete.and.returnValue(of(null));
    service.delete(articles[0]._id!.toString()).subscribe((result) => {
      expect(result).toEqual(null!)
      expect(httpSpy.delete).toHaveBeenCalled();
      done();
    });
  });

});
