import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { forkJoin, of, switchMap } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { User } from '../../user/user.model';
import { Article, Category } from '../article.model';
import { ArticleService } from '../article.service';

@Component({
  selector: 'app-article-form',
  templateUrl: './article-form.component.html',
  styleUrls: ['./article-form.component.css']
})
export class ArticleFormComponent implements OnInit {

  article: Article | null = null;
  currentuser?: User;
  title!: String;
  submitted = false;
  categories = Object.values(Category);
  articleForm = this.formBuilder.group({
    _id: [''],
    title: ['', Validators.required],
    subtitle: ['', Validators.required],
    content: ['', Validators.required],
    category: ['', Validators.required],
    user: [''],
    banner: ['']
  });

  get f() {return this.articleForm.controls;}
  options?: Object;

  constructor(
    private route: ActivatedRoute,
    private articleService: ArticleService,
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService
    ) { }

  ngOnInit(): void {
    this.getArticle();
  }

  onSubmit(): void {
    this.submitted = true;
    if(this.articleForm.invalid) {
      return;
    }

    if(this.article) {
      console.log(this.currentuser?.token)

      this.articleService.update(this.articleForm.value, this.options).subscribe(() => {
        this.router.navigate(['articles', this.article!._id]);
      });
    } else {
      this.articleForm.removeControl('_id')
      this.articleForm.patchValue({user: this.currentuser!._id!.toString()});
      console.log('KAAAAAAAS', this.options)
      this.articleService.create(this.articleForm.value, this.options).subscribe(() => {
          console.log("new article: ", this.articleForm.value);
          this.router.navigate(['articles']);
        }
      );
    }
  }

  onDelete() {
    if(this.article?._id) {
      this.articleService.delete(this.article._id?.toString(), this.options).subscribe(() => {
        this.router.navigate(['articles']);
      });
      
    }
  }

  private getArticle() {
    this.route.paramMap
    .pipe(
      switchMap((params: ParamMap) => {
        if(!params.get('articleId')) {
          return of(null)
        } else {
          return this.articleService.read(params.get('articleId') ?? 0);
        }
      })
    )
    .subscribe(article => {
      this.article = article;
      if(article) {
        this.articleForm.patchValue({_id: article._id})
        this.articleForm.patchValue(article);
      }
      this.title = article === null ? 'Create article' : 'Edit article';
      this.getCurrentUser() 
    });
  }

  private getCurrentUser() {
    this.authService.currentUser$.subscribe((r) => {
      this.currentuser = r;
      if(this.currentuser) {
        this.options = {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + this.currentuser?.token
          })
        }
        
        if(this.article) {
          this.authService.userMayEdit(this.article!.user!._id!.toString()).subscribe(res => {
            if(!res) {
              console.log("user may not edit")
              this.router.navigate(['/'])
            } else {
              console.log("user may edit")
            }; 
          })
        }
      }
    })
  }
}
