import { HttpClientModule } from "@angular/common/http";
import { Directive, HostListener, Input } from "@angular/core";
import { ComponentFixture, TestBed } from "@angular/core/testing";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ActivatedRoute, convertToParamMap } from "@angular/router";
import { RouterTestingModule } from "@angular/router/testing";
import { of } from "rxjs";
import { AuthService } from "src/app/auth/auth.service";
import { Role, User } from "../../user/user.model";
import { Article, Category } from "../article.model";
import { ArticleService } from "../article.service";
import { ArticleFormComponent } from "./article-form.component";

//Mocking the routeLink directive
@Directive({
    selector: '[routerLink]'
})

export class RouterLinkStubDirective {
    @Input('routerLink') linkParams: any;
    navigatedTo: any = null;

    @HostListener('click')
    onClick(): void {
        this.navigatedTo = this.linkParams;
    }
}

const article: Article = 
{
    _id: "1",
    user: {
        _id: "123",
        token:"mytoken",
        birthdate: new Date(),
        email: "email@email.com",
        games: [],
        lists: [],
        password: "password", 
        role: Role.creator,
        username: "Creator",
        profilePicture: "",
    },
    title: "testarticle",
    subtitle: "this is a subtitle",
    content: "this is the content",
    date: new Date(),
    category: Category.news,
    banner: ""
}

describe('ArticleFormComponent', () => {
    let component: ArticleFormComponent;
    let fixture: ComponentFixture<ArticleFormComponent>;
    
    let articleServiceSpy: any;
    let authServiceSpy: any;

    beforeEach(() => {
        articleServiceSpy = jasmine.createSpyObj('ArticleService', ['read', 'update', 'delete', 'list']);
        authServiceSpy = jasmine.createSpyObj('AuthService', ['userMayEdit'], {currentUser$: of(article.user)});
        
        TestBed.configureTestingModule({
            declarations: [
                ArticleFormComponent,
                RouterLinkStubDirective
            ],
            imports: [
                ReactiveFormsModule,
                FormsModule,
                RouterTestingModule,
                HttpClientModule
            ],
            providers: [
                {provide: ArticleService, useValue: articleServiceSpy},
                {provide: AuthService, useValue: authServiceSpy},
                {
                    provide: ActivatedRoute,
                    useValue: {
                        paramMap: of(
                            convertToParamMap({
                                articleId: "1"
                            })
                        )
                    }
                }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(ArticleFormComponent);
        component = fixture.componentInstance;
    });

    afterEach(() => {
        fixture.destroy();
    })
  
    it('Should load article data and have "Edit article" as title', () => {
        articleServiceSpy.read.and.returnValue(of(article))
        authServiceSpy.userMayEdit.and.returnValue(of(true));
        fixture.detectChanges();

        expect(component.article).toEqual(article);
        expect(component.title).toEqual("Edit article")
        expect(component.articleForm.value._id).toEqual(article._id);
        expect(component.articleForm.value.title).toEqual(article.title);
        expect(component.articleForm.value.subtitle).toEqual(article.subtitle);
        expect(component).toBeTruthy();
    });

    it('Should load a new articleForm and have "Create article" as title', () => {
        articleServiceSpy.read.and.returnValue(of(null))
        fixture.detectChanges();
        
        expect(component.title).toEqual("Create article")
        expect(component.articleForm.value._id).toEqual('');
        expect(component.articleForm.value.title).toEqual('');
        expect(component.articleForm.value.subtitle).toEqual('');
        expect(component).toBeTruthy();
    });
});