import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EntityService } from 'src/app/shared/entity.service';
import { environment } from 'src/environments/environment';
import { Article } from './article.model';

@Injectable({
  providedIn: 'root',
})
export class ArticleService extends EntityService<Article> {

  
  constructor(http: HttpClient) { 
    super(http, environment.API_URL, 'article')
  }
}
