import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AlertService } from 'src/app/shared/alert/alert.service';
import { Article } from '../article.model';
import { ArticleService } from '../article.service';

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.css']
})
export class ArticleListComponent implements OnInit, OnDestroy {
  articles: Article[] | null = null;
  
  articleSubscription?: Subscription

  constructor(
    private articleService : ArticleService,
    private alertService: AlertService
  ) {}
  
  ngOnInit(): void {
    this.articleSubscription = this.articleService.list()
      .subscribe(
        articles => this.articles = articles,
        err => this.alertService.error("Server is currently offline, please try again later.")
      );
  }

  // Search filter
  onSearch(event: any) {
    console.log(event)
    const articleList = document.querySelectorAll(".listItem");
    const term = event.target.value.toLowerCase();
    Array.from(articleList).forEach((article) => {
      const title = article.querySelector(".card-title")?.textContent?.toLocaleLowerCase();
      const wrapper = article.parentElement;
      if(title?.indexOf(term) != -1) {
        article.setAttribute("class", "mt-5 listItem")
        if(term == "") {
          wrapper?.setAttribute("class", "col-6 d-flex justify-content-center wrapper")
        } else {
          wrapper?.setAttribute("class", "col-12 d-flex justify-content-center wrapper")
        }
      } else {
        article.setAttribute("class", "mt-5 listItem d-none")
        wrapper?.setAttribute("class", "col-6 d-flex justify-content-center wrapper")
      }
    })    
  }

  ngOnDestroy(): void {
    this.articleSubscription?.unsubscribe();
  }

}
