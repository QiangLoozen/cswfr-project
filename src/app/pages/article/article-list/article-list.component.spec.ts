import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleListComponent } from './article-list.component';

import { Directive, HostListener, Input } from '@angular/core';
import { Article, Category } from '../article.model';
import { ArticleService } from '../article.service';
import { of, Subscription } from 'rxjs';
import { Role } from '../../user/user.model';


//Mocking the routeLink directive
@Directive({
  selector: '[routerLink]'
})
export class RouterLinkStubDirective {
  @Input('routerLink') linkParams: any;
  navigatedTo: any = null;

  @HostListener('click')
  onClick(): void {
    this.navigatedTo = this.linkParams;
  }
}

//Mocking the response from the articleservice
const articles: Article[] = [
  {
    _id: "1",
    user: {
      _id: "123",
      token:"mytoken",
      birthdate: new Date(),
      email: "email@email.com",
      games: [],
      lists: [],
      password: "password", 
      role: Role.creator,
      username: "Creator",
      profilePicture: "",
    },
    title: "testarticle",
    subtitle: "this is a subtitle",
    content: "this is the content",
    date: new Date(),
    category: Category.news,
    banner: ""
  }
]

describe('ArticleListComponent', () => {
  let component: ArticleListComponent;
  let fixture: ComponentFixture<ArticleListComponent>;
  
  //Mock objects
  
  let articleServiceSpy: any;
  beforeEach(() => {
    articleServiceSpy = jasmine.createSpyObj('ArticleService', ['list']);

    TestBed.configureTestingModule({
      declarations: [
        ArticleListComponent,
        RouterLinkStubDirective
      ],
      imports: [],
      providers: [{provide: ArticleService, useValue: articleServiceSpy}]
    }).compileComponents();

    fixture = TestBed.createComponent(ArticleListComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => {
    fixture.destroy();
  });
  
  it('Should load list of articles', (done: DoneFn) => {
    articleServiceSpy.list.and.returnValue(of(articles))
    fixture.detectChanges();

    component.articleSubscription = new Subscription();

    expect(component.articles).toEqual(articles);
    expect(component).toBeTruthy();
    done()
  });
});
