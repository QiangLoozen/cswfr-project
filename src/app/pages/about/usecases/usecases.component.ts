import { Component, Input, OnInit } from '@angular/core';
import { UseCase } from './usecase.model';

@Component({
  selector: 'app-usecases',
  templateUrl: './usecases.component.html',
  styles: [
  ]
})
export class UsecasesComponent implements OnInit {
  @Input() useCase: UseCase | undefined
  constructor() { }

  ngOnInit(): void {
  }

}
