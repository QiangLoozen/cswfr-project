import { Component, OnInit } from '@angular/core';
import { UseCase } from './usecases/usecase.model';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  readonly CUSTOMER = 'Customer'
  readonly CREATOR = 'Creator'
  readonly REGULAR = 'Regular user'

  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.'
      ],
      actor: this.CUSTOMER,
      precondition: 'Geen',
      postcondition: 'De actor is ingelogd'
    },
    {
      id: 'UC-02',
      name: 'Registreren',
      description: 'Een bezoeker kan registreren op de website',
      scenario: [
        'De gebruiker vult zijn gegevens in en kiest of hij customer wilt zijn of creator', 
        'De applicatie valideert de ingevoerde gegevens', 
        'Indien de gegevens correct zijn dan redirect de applicatie naar het startscherm'
      ],
      actor: this.REGULAR,
      precondition: '-',
      postcondition: 'Het doel is bereikt.'
    },
    {
      id: 'UC-03',
      name: 'Gebruikersgegevens wijzigen',
      description: 'Een gebruiker kan zijn gegevens wijzigen',
      scenario: [
        'De gebruiker vult zijn gegevens in.', 
        'De applicatie valideert de ingevoerde gegevens', 
        'Indien de gegevens correct zijn dan redirect de applicatie naar de detailpagina van een gebruiker'
      ],
      actor: this.CUSTOMER,
      precondition: 'De actor is ingelogd',
      postcondition: 'De gegevens van de gebruiker is veranderd'
    },
    {
      id: 'UC-04',
      name: 'Games bekijken en zoeken',
      description: 'Een bezoeker kan games bekijken en zoeken',
      scenario: [
        'De bezoeker navigeert naar de games pagina', 
        'De bezoeker zoekt een game in de lijst', 
        'De game is gevonden en de gebruiker kan de details opvragen',
        'Alle informatie van de game is opgehaald'
      ],
      actor: this.REGULAR,
      precondition: '-',
      postcondition: 'De gegevens van de gebruiker is veranderd'
    },
    {
      id: 'UC-05',
      name: 'Gebruiker kan een game kopen',
      description: 'Een gebruiker kan een game kopen',
      scenario: [
        'De gebruiker zoekt een game en vraagt de details op', 
        'De gebruiker drukt op de "add to chart" button', 
        'De chart/winkelmandje wordt bijgewerkt',
        'De gebruiker klikt op het winkelmandje',
        'De gebruiker koopt de game',
        'Na de betaling redirect de website naar een bevestigingsscherm'
      ],
      actor: this.CUSTOMER,
      precondition: 'De actor is ingelogd',
      postcondition: 'De actor bezit nu over de game'
    },
    {
      id: 'UC-06',
      name: 'Gebruiker kan een lijst toevoegen',
      description: 'Een gebruiker kan een lijst aanmaken waar games in kunnen worden bewaard',
      scenario: [
        'De gebruiker gaat naar zijn profiel', 
        'De gebruiker gaat naar zijn lijsten', 
        'De gebruiker voegt een nieuwe lijst toe',
      ],
      actor: this.CUSTOMER,
      precondition: 'De actor is ingelogd',
      postcondition: 'Er is een lege lijst toegevoegd aan de gebruiker'
    },
    {
      id: 'UC-07',
      name: 'Bezoeker kan artikelen lezen',
      description: 'Een bezoeker kan artikelen lezen',
      scenario: [
        'De bezoeker navigeert naar news', 
        'De bezoeker bekijkt een lijst met nieuwsartikelen', 
        'De bezoeker kan een nieuwsartikel bekijken',
      ],
      actor: this.REGULAR,
      precondition: '-',
      postcondition: 'Artikelen en details kunnen worden gelezen'
    },
    {
      id: 'UC-08',
      name: 'Gebruiker kan een recentie plaatsen onder een game',
      description: 'Gebruiker kan een recentie plaatsen onder een game',
      scenario: [
        'De gebruiker zoekt een game en vraagt de details op', 
        'De gebruiker maakt een nieuwe review', 
        'De website valideert de review',
        'De review is geplaatst'
      ],
      actor: this.CUSTOMER,
      precondition: 'De actor is ingelogd',
      postcondition: 'De review is toegevoegd aan de game en kan worden gelezen'
    },
    {
      id: 'UC-09',
      name: 'Gebruiker kan een vriendschapsverzoek versturen',
      description: 'Gebruiker kan een vriendschapsverzoek versturen naar andere gebruikers',
      scenario: [
        'De gebruiker zoekt een gebruiker op', 
        'De gebruiker bekijkt de details van de gebruiker', 
        'De gebruiker verstuurd een vriendschapsverzoek'
      ],
      actor: this.CUSTOMER,
      precondition: 'De actor is ingelogd',
      postcondition: 'De vriendschapsverzoek is verstuurd naar de andere gebruiker'
    },
    {
      id: 'UC-10',
      name: 'Gebruiker kan vriendschapsverzoeken accepteren of afwijzen',
      description: 'Gebruiker kan vriendschapsverzoeken accepteren of afwijzen',
      scenario: [
        'De gebruiker navigeert naar zijn eigen profiel', 
        'De gebruiker navigeert naar zijn vrienden', 
        'De gebruiker heeft een vriendschapsverzoek staan',
        'De gebruiker accepteert of wijst het vriendschapsverzoek af'
      ],
      actor: this.CUSTOMER,
      precondition: 'De actor is ingelogd',
      postcondition: 'Verzoek is afgewezen of de actor is nu bevriend'
    },
    {
      id: 'UC-11',
      name: 'Creator kan games toevoegen of wijzigen',
      description: 'Creator kan games toevoegen',
      scenario: [
        'De gebruiker maakt een nieuwe, of wijzigt een game', 
        'De website valideert de gegevens', 
        'De website reditect de gebruiker naar de game pagina',
      ],
      actor: this.CREATOR,
      precondition: 'De actor is ingelogd en is een creator. actor is owner',
      postcondition: 'De game is aangemaakt en kan worden opgehaald'
    },
    {
      id: 'UC-12',
      name: 'Creator kan artikelen toevoegen of wijzigen',
      description: 'Creator kan artikelen toevoegen',
      scenario: [
        'De gebruiker maakt een nieuwe, of wijzigt een artikel', 
        'De website valideert de gegevens', 
        'De website reditect de gebruiker naar de artikel pagina',
      ],
      actor: this.CREATOR,
      precondition: 'De actor is ingelogd en is een creator, actor is owner',
      postcondition: 'Het artikel is aangemaakt en kan worden opgehaald'
    },
    
    


  ]
  
  constructor() { }

  ngOnInit(): void {
  }

}
