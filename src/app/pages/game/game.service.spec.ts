import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { Game } from './game.model';

import { GameService } from './game.service';

const games: Game[] = [
  {
    _id: "1",
    trailer_url: "https://www.youtube.com/embed/ASzOzrB-a9E",
    release_date: new Date(),
    long_description: "longdescription...",
    short_description: "shortdescription...",
    price: 59.99,
    name: "battlefield 2042",
    reviews: [
    ],
    category: "action",
    publisher: "Electronic Arts",
    banner: ""
  }
]

describe('GameService', () => {
  let service: GameService;
  let httpSpy : jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    httpSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);

    TestBed.configureTestingModule({
      providers: [{provide: HttpClient, useValue: httpSpy}]
    });
    service = TestBed.inject(GameService);
    httpSpy = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
  });

  it('Should return a list of games', (done: DoneFn) => {
    httpSpy.get.and.returnValue(of(games));
    service.list().subscribe((result) => {
      expect(result!.length).toBe(1);
      expect(result![0]._id).toEqual(games[0]._id);
      expect(service).toBeTruthy();
      done();
    });
  });
});
