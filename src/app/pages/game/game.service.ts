import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, tap } from 'rxjs';
import { EntityService } from 'src/app/shared/entity.service';
import { environment } from 'src/environments/environment';
import { Game } from './game.model';


@Injectable({
  providedIn: 'root'
})
export class GameService extends EntityService<Game> {

  private readonly headers = new HttpHeaders({
    'Content-Type': 'application/json',
  });

  constructor(http: HttpClient) { 
    super(http, environment.API_URL, 'game');
  }

  public purchase(gameId: string, userId: string): Observable<Object> {
    const endpoint = `${environment.API_URL}game/${gameId}/purchase`;
    console.log(`post ${endpoint}`);

    return this.http.post(endpoint, {userId: userId}, { headers: this.headers}).pipe(
      tap(console.log),
      catchError(this.handleError)
    )
  }

  public getRecommendedGames(userId: string): Observable<Game[]> {
    const endpoint = `${environment.API_URL}user/${userId}/recommendations/similarGames`;
    console.log(`get ${endpoint}`);

    return this.http.get(endpoint).pipe(
      tap(console.log),
      catchError(this.handleError)
    )
  }
}
