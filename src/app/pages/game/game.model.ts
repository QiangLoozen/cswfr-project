import { Entity } from "src/app/shared/entity.model";
import { Review } from "../review/review.model";
import { User } from "../user/user.model";

export enum GameCategory {
    action = "action",
    RPG = "RPG",
    horror = "horror",
    strategy = "strategy",
    adventure = "adventure",
    casual = "casual",
    survival = "survival"
}

export class Game extends Entity {
    name: string = '';
    price: Number = 0;
    short_description: string = '';
    long_description: string = '';
    release_date: Date = new Date();
    trailer_url: string = '';
    reviews: Review[] = [];
    category: string = '';
    publisher: string = '';
    createdBy?: User;
    banner: string = '';
}