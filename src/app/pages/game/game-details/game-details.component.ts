import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { switchMap } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { AlertService } from 'src/app/shared/alert/alert.service';
import { Review } from '../../review/review.model';
import { ReviewService } from '../../review/review.service';
import { ListService } from '../../user/list.service';
import { List, User } from '../../user/user.model';
import { Game } from '../game.model';
import { GameService } from '../game.service';

@Component({
  selector: 'app-game-details',
  templateUrl: './game-details.component.html',
  styleUrls: ['./game-details.component.css']
})
export class GameDetailsComponent implements OnInit {

  isReadMore = false
  game: Game | null = null;
  currentuser?: User;
  userLists?: List[];

  options?: Object;

  constructor(    
    private gameService: GameService,
    private activatedRoute: ActivatedRoute,
    public authService: AuthService,
    private listService: ListService,
    private router: Router,
    private reviewService: ReviewService,
    private alertService: AlertService
    ) { }

  ngOnInit(): void {
    this.getGameDetails()
  }

  onPurchase() {
    if(!this.game ||  !this.currentuser){
      console.error("Failed to retrieve needed data for purchase")
    } else {
      let bought = false
      console.log("CUURENT USER",this.currentuser)
      this.currentuser?.games.forEach((game) => {
        console.log("bought: " + game._id?.toString() + " current game: "+ this.game?._id?.toString())
        if(game._id?.toString() == this.game?._id?.toString()) {
          bought = true;
        }
      })

      if(bought) {
        this.alertService.error('Already bought the game')
      } else {
        this.gameService.purchase(this.game!._id!.toString(), this.currentuser!._id!.toString()).subscribe(() => {
          this.alertService.success('Succesfully bought the game');
          this.currentuser?.games.push(this.game!);
          this.authService.currentUser$.next(this.currentuser!)
          this.getCurrentUser();
        })
      }
    }

  }

  onAddToList(listName?: string) {
    console.log("ADDDDD", this.options)
    this.listService.addToList(this.currentuser!._id!.toString(), listName!, this.game!._id!.toString(), this.options).subscribe(() => {
      this.router.navigate([`/users/${this.currentuser!._id!.toString()}/lists/${listName!}`])
    })
  }

  onLike(review: Review) {
    if(!this.game || !review || !this.currentuser) {
      console.error("Failed to retrieve needed data for like");
    } else {
      this.reviewService.addLike(this.game!._id!.toString(), review._id!.toString(), this.currentuser!._id!.toString()).subscribe(() => {
        this.getGameDetails();
      })
    }
  }
  onDislike(review: Review) {
    if(!this.game || !review || !this.currentuser) {
      console.error("Failed to retrieve needed data for like");
    } else {
      this.reviewService.removeLike(this.game!._id!.toString(), review._id!.toString(), this.currentuser!._id!.toString()).subscribe(() => {
        this.getGameDetails();
      })
    }
  }

  private getGameDetails() {
    this.authService.currentUser$.subscribe(cu => {
      this.currentuser = cu;
      this.options = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + this.currentuser?.token
        })
      } 

      this.activatedRoute.paramMap.pipe(
        switchMap((params: ParamMap) => {
          return this.gameService.read(params.get('gameId') ?? 0)
          }
        )
      ).subscribe(game => {
        this.game = game
        this.getUserLists();
      })

    });
  }
  private getCurrentUser() {
    this.authService.currentUser$.subscribe((r) => {
      this.currentuser = r;
    })
  }

  private getUserLists() {
    if(this.currentuser?._id) {
      this.listService.getLists(this.currentuser!._id!.toString(), this.options).subscribe((r) => {
        this.userLists = r;
      })
    }
  }


  showText() {
     this.isReadMore = !this.isReadMore
  }

}
