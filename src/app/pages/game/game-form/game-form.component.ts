import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { of, switchMap } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { User } from '../../user/user.model';
import { Game, GameCategory } from '../game.model';
import { GameService } from '../game.service';

@Component({
  selector: 'app-game-form',
  templateUrl: './game-form.component.html',
  styleUrls: ['./game-form.component.css']
})
export class GameFormComponent implements OnInit {

  game: Game | null = null;
  currentuser?: User;
  title!: String;
  submitted = false;
  gameCategories = Object.values(GameCategory);
  gameForm = this.formBuilder.group({
    _id: [''],
    name: ['', Validators.required],
    price: ['', [Validators.required, Validators.min(0)]],
    short_description: ['', Validators.required],
    long_description: ['', Validators.required],
    release_date: ['', Validators.required],
    trailer_url: ['', Validators.required],
    category: ['', Validators.required],
    publisher: ['', Validators.required],
    createdBy: [''],
    banner: ['']
  });

  options?: object;

  constructor(
    private activatedRoute: ActivatedRoute,
    private gameService: GameService,
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap
      .pipe(
        switchMap((params: ParamMap) => { // GET GAME ID
          if(!params.get('gameId')) {
            return of(null)
          } else {
            return this.gameService.read(params.get('gameId') ?? 0)
          }
        }),
        switchMap(game => { // GET GAME DETAILS
          this.game = game
          if(game) {
            this.title = "Edit game"
            let date = this.game!.release_date.toString().split('T')[0];
            this.gameForm.patchValue(game);
            this.gameForm.patchValue({release_date: date});
          } else {
            this.title = "Create game"
            this.gameForm.patchValue({trailer_url: "https://www.youtube.com/embed/ubJVD1JeYMw"})
          }
  
          return this.authService.currentUser$
        })
      ).subscribe(cu => { // GET CURRENT USER FOR AUTHORISATION
        this.currentuser = cu;

        // Adding currentuser token to headers
        this.options = {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + this.currentuser?.token
          })
        } 

        // Check if user may edit
        if(this.game && this.currentuser) {
          this.authService.userMayEdit(this.game!.createdBy!._id!.toString()).subscribe(res => {
            if(!res) {
              console.log("user may not edit")
              this.router.navigate(['/'])
            } else {
              console.log("user may edit")
            }; 
          })
        }
      });
  }

  onSubmit(): void {
    this.submitted = true;
    if(this.gameForm.invalid) {
      return;
    }
    let submittedGame = this.gameForm.value;
    console.log(this.options)
    if(this.game) {
      this.gameService.update(submittedGame, this.options).subscribe(() => {
        this.router.navigate(['games', this.game!._id]);
      });
    } else {
      delete submittedGame._id;
      submittedGame.createdBy = this.currentuser?._id;

      this.gameService.create(submittedGame, this.options).subscribe(() => {
          console.log("new game: ", submittedGame);
          this.router.navigate(['/']);
        }
      );
    }
  }
  onDelete() {
    if(this.game?._id) {
      this.gameService.delete(this.game._id?.toString(), this.options).subscribe(() => {
        this.router.navigate(['/']);
      });
      
    }
  }

  get f() {return this.gameForm.controls;}

}
