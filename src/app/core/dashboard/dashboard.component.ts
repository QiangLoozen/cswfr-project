import { Component, OnDestroy, OnInit } from '@angular/core';
import { flatMap, of, Subscription, switchMap } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { Game, GameCategory } from 'src/app/pages/game/game.model';
import { GameService } from 'src/app/pages/game/game.service';
import { User } from 'src/app/pages/user/user.model';
import { AlertService } from 'src/app/shared/alert/alert.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {

  // Website information
  runningMode: string = ''
  apiUrl: string = ''
  version: string = ''

  // Banner
  images = ['image1.png', 'image2.png', 'image3.png'].map((n) => `assets/images/carousel/${n}`);
  pauseOnHover = true;

  // Data
  games: Game[] | null = null;
  recommended?: Game[];
  currentuser?: User;
  gameCategories = Object.values(GameCategory)

  // Subscriptions
  gameSubscription?: Subscription;
  cuSubscription?: Subscription;

  constructor(
    private gameService: GameService,
    private authService: AuthService,
    private alertService: AlertService
    ) {}

  ngOnInit(): void {
    this.runningMode = environment.production ? 'production' : 'development'
    this.apiUrl = environment.API_URL
    this.version = environment.version


    this.gameSubscription = this.gameService.list().subscribe(
        games => {this.games = games},
        err => this.alertService.error("Server is currently offline, please try again later.")
      );

    this.cuSubscription = this.authService.currentUser$
      .pipe(
        switchMap(result => {
          this.currentuser = result;
          if(this.currentuser?._id) {
            return this.gameService.getRecommendedGames(this.currentuser._id.toString())
          } else {
            return of(null);
          }
        })
      )
      .subscribe((recGames) => {
        recGames ? this.recommended = recGames : this.recommended = [];
      })
  }

  // DOM filters
  onSearch(event: any) {
    const gameList = document.querySelectorAll(".listItem");
    const term = event.target.value.toLowerCase();
    Array.from(gameList).forEach((game) => {
      const title = game.querySelector(".card-title")?.textContent?.toLocaleLowerCase();
      if(title?.indexOf(term) != -1) {
        game.setAttribute("class", "listItem")
      } else {
        game.setAttribute("class", "listItem d-none")
      }
    })    
  }

  onCategoryChange(event: any) {
    const gameList = document.querySelectorAll(".listItem");
    const term = event.target.value;
    Array.from(gameList).forEach((game) => {
      const category = game.querySelector(".gameCategory")?.textContent;
      if(category == term || term == 'all') {
        game.setAttribute("class", "listItem")
      } else {
        game.setAttribute("class", "listItem d-none")
      }
    })  
  }
  
  onShowRecommendations(event: any) {
    const recommendations = document.querySelector(".recommendations");
    const checked = event.target.checked;
    if(checked) {
      recommendations?.setAttribute("class", "recommendations")
    } else {
      recommendations?.setAttribute("class", "recommendations d-none")
    }
  }

  ngOnDestroy(): void {
    this.gameSubscription?.unsubscribe();
    this.cuSubscription?.unsubscribe();
  }

}
