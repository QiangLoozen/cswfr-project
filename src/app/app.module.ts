import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { RouterModule } from '@angular/router'
import { HttpClientModule} from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { NavbarComponent } from './core/navbar/navbar.component'
import { LayoutComponent } from './core/layout/layout.component'
import { FooterComponent } from './core/footer/footer.component'
import { AboutComponent } from './pages/about/about.component'
import { UsecasesComponent } from './pages/about/usecases/usecases.component';
import { UserListComponent } from './pages/user/user-list/user-list.component';
import { UserDetailComponent } from './pages/user/user-detail/user-detail.component';
import { UserEditComponent } from './pages/user/user-edit/user-edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GameDetailsComponent } from './pages/game/game-details/game-details.component';
import { ArticleDetailComponent } from './pages/article/article-detail/article-detail.component';
import { ArticleListComponent } from './pages/article/article-list/article-list.component';
import { ArticleFormComponent } from './pages/article/article-form/article-form.component';
import { UserGamesComponent } from './pages/user/user-detail/user-games/user-games.component';
import { UserFriendsComponent } from './pages/user/user-detail/user-friends/user-friends.component';
import { UserListsComponent } from './pages/user/user-detail/user-lists/user-lists.component';
import { NotfoundComponent } from './core/notfound/notfound.component';
import { UserListsDetailsComponent } from './pages/user/user-detail/user-lists-details/user-lists-details.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { SafePipe } from './shared/safe.pipe';
import { GameFormComponent } from './pages/game/game-form/game-form.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { AlertComponent } from './shared/alert/alert.component';
import { UserListFormComponent } from './pages/user/user-detail/user-list-form/user-list-form.component';
import { ReviewFormComponent } from './pages/review/review-form/review-form.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LayoutComponent,
    DashboardComponent,
    FooterComponent,
    AboutComponent,
    UsecasesComponent,
    UserListComponent,
    UserDetailComponent,
    UserEditComponent,
    GameDetailsComponent,
    ArticleDetailComponent,
    ArticleListComponent,
    ArticleFormComponent,
    UserGamesComponent,
    UserFriendsComponent,
    UserListsComponent,
    NotfoundComponent,
    UserListsDetailsComponent,
    SpinnerComponent,
    SafePipe,
    GameFormComponent,
    LoginComponent,
    RegisterComponent,
    AlertComponent,
    UserListFormComponent,
    ReviewFormComponent,
  ],
  imports: [BrowserModule, HttpClientModule, RouterModule, NgbModule, AppRoutingModule, FormsModule, ReactiveFormsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
