import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { AuthGuard } from './auth/auth.guard'
import { CreateGuard } from './auth/create.guard'
import { LoginComponent } from './auth/login/login.component'
import { RegisterComponent } from './auth/register/register.component'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { LayoutComponent } from './core/layout/layout.component'
import { NotfoundComponent } from './core/notfound/notfound.component'
import { AboutComponent } from './pages/about/about.component'
import { ArticleDetailComponent } from './pages/article/article-detail/article-detail.component'
import { ArticleFormComponent } from './pages/article/article-form/article-form.component'
import { ArticleListComponent } from './pages/article/article-list/article-list.component'
import { GameDetailsComponent } from './pages/game/game-details/game-details.component'
import { GameFormComponent } from './pages/game/game-form/game-form.component'
import { ReviewFormComponent } from './pages/review/review-form/review-form.component'
import { UserDetailComponent } from './pages/user/user-detail/user-detail.component'
import { UserFriendsComponent } from './pages/user/user-detail/user-friends/user-friends.component'
import { UserGamesComponent } from './pages/user/user-detail/user-games/user-games.component'
import { UserListFormComponent } from './pages/user/user-detail/user-list-form/user-list-form.component'
import { UserListsDetailsComponent } from './pages/user/user-detail/user-lists-details/user-lists-details.component'
import { UserListsComponent } from './pages/user/user-detail/user-lists/user-lists.component'
import { UserEditComponent } from './pages/user/user-edit/user-edit.component'
import { UserListComponent } from './pages/user/user-list/user-list.component'

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'dashboard' },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'about', component: AboutComponent },
      { path: 'login', component: LoginComponent},
      { path: 'register', component: RegisterComponent},
      { path: 'users', canActivate: [AuthGuard], component: UserListComponent},
      { path: 'users/:userId',canActivate: [AuthGuard], component: UserDetailComponent, children: [
        { path: '', pathMatch: 'full', redirectTo: 'games'},
        { path: 'games', pathMatch: 'full', canActivate: [AuthGuard], component: UserGamesComponent},
        { path: 'lists', pathMatch: 'full', canActivate: [AuthGuard], component: UserListsComponent},
        { path: 'lists/create', pathMatch: 'full', canActivate:[AuthGuard], component: UserListFormComponent},
        { path: 'lists/:listId', pathMatch: 'full', canActivate: [AuthGuard], component: UserListsDetailsComponent},
        { path: 'lists/:listId/edit', pathMatch: 'full', canActivate: [AuthGuard], component: UserListFormComponent},
        { path: 'friends', pathMatch: 'full', canActivate: [AuthGuard], component: UserFriendsComponent}
      ]},
      { path: 'users/:userId/edit', pathMatch: 'full', canActivate: [AuthGuard], component: UserEditComponent},
      { path: 'games/create', pathMatch: 'full', canActivate:[CreateGuard], component: GameFormComponent},
      { path: 'games/:gameId', pathMatch: 'full', component: GameDetailsComponent},
      { path: 'games/:gameId/edit', pathMatch: 'full', canActivate: [AuthGuard], component: GameFormComponent},
      { path: 'games/:gameId/review/create', pathMatch: 'full',canActivate: [AuthGuard], component: ReviewFormComponent},
      { path: 'games/:gameId/review/:reviewId/edit', pathMatch: 'full', canActivate: [AuthGuard], component: ReviewFormComponent},
      { path: 'articles', pathMatch: 'full', component: ArticleListComponent},
      { path: 'articles/create', pathMatch: 'full', canActivate:[CreateGuard], component: ArticleFormComponent},
      { path: 'articles/:articleId', pathMatch: 'full', component: ArticleDetailComponent},
      { path: 'articles/:articleId/edit', pathMatch: 'full', canActivate: [AuthGuard], component: ArticleFormComponent}
    ]
  },
  { path: '404', component: NotfoundComponent },
  { path: '**', redirectTo: '/404'}
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
