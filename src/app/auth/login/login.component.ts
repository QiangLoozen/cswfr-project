import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  submitted = false;

  loginForm = this.fb.group({
    email: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
    password: ['', Validators.required]
  });

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService
    ) { }

  ngOnInit(): void {
  }

  onSubmit() {
    this.submitted = true;
    if(this.loginForm.valid) {
      let email: string = this.loginForm.value.email;
      let formattedEmail = email.trim().toLowerCase();
      const password = this.loginForm.value.password;

      this.authService.login(formattedEmail, password).subscribe((result) => {
        if(result) {
          console.log('Logged In');
          this.router.navigate(['/'])
        } else {
          console.error('loginForm invalid');
        }
      })
    }
  }

  get f() {return this.loginForm.controls};
}
