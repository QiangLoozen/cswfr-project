import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Role, User } from 'src/app/pages/user/user.model';
import { UserService } from 'src/app/pages/user/user.service';
import { confirmPassword, futureDateValidator } from 'src/app/pages/user/user.validation';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  users: User[] = [];
  roles = Object.values(Role);
  submitted = false;
  userEmails?: string[];
  get f() {return this.registerForm.controls;}

  registerForm = this.formBuilder.group({
    username: ['', Validators.required],
    email: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
    role: ['', Validators.required],
    birthdate: ['', [Validators.required, futureDateValidator()]],
    password: ['', [Validators.required, Validators.minLength(7)]],
    confirmPassword: ['', [Validators.required , Validators.minLength(7)]],
    profilePicture: ['']
  }, {
    validator: confirmPassword('password', 'confirmPassword')
  }) 

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.getAllEmails();
  }

  onSubmit(): void {
    this.submitted = true;
    if(this.registerForm.valid) {
      let registerData = this.registerForm.value;
      let email: string = registerData.email
      const formattedEmail = email.trim().toLowerCase();
      registerData.email = formattedEmail;

      let emailExist = false;
      this.userEmails?.forEach((item) => {
        if(item === formattedEmail) {
          this.registerForm.controls.email.setErrors({alreadyExist: true})
          console.warn(`Email: ${formattedEmail} already exist!`)
          emailExist = true;
        }
      })
      
      if(!emailExist) {
        this.authService.register(registerData).subscribe(() => {
          this.router.navigate(['users']);
        });
      }
    }
  }

  private getAllEmails() {
    this.userService.getAllEmails().subscribe((r) => {
      this.userEmails = r
    })
  }
}
