import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { map, Observable } from 'rxjs';
import { User } from '../pages/user/user.model';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})

export class CreateGuard implements CanActivate {
  constructor(
    private authService: AuthService, 
    private router: Router){}
    
  canActivate(): Observable<boolean> {
    console.log('canCreate LoggedIn');
    return this.authService.currentUser$.pipe(
      map((user: User) => {
        if (user && user.role == "creator") {
          return true;
        } else {
          console.log('Not a creator');
          this.router.navigate(['/']);
          return false;
        }
      })
    );
  }
  
}
