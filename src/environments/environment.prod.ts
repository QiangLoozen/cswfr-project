const packagejson = require('../../package.json')

export const environment = {
  production: true,

  // Fill in your own online server API url here
  API_URL: 'https://cswf-qiang-api.herokuapp.com/api/',

  version: packagejson.version
}
